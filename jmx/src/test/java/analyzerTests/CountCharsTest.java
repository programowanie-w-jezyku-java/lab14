import classes.analyzer.CountCharsAnalyzer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CountCharsTest extends AnalyzerTest {
    private static String EMPTY_FILE = "Pusty plik!";

    @Test
    @DisplayName("analyzer should explode on null data")
    void explodeOnNull() {
        var analyzer = new CountCharsAnalyzer(null);
        assertThrows(NullPointerException.class, analyzer::analyze);
    }

    @Test
    @DisplayName("analyzer should return 0 on empty file")
    void whenFileIsEmptyReturn0() {
        var analyzer = new CountCharsAnalyzer(emptyFile);
        assertEquals(EMPTY_FILE, analyzer.analyze());
    }

    @Test
    @DisplayName("should return correct lines count for each file")
    void countDataCorrectly() {
        var analyzer = new CountCharsAnalyzer(threeEmptyLines);
        assertEquals(EMPTY_FILE, analyzer.analyze());

        analyzer = new CountCharsAnalyzer(threeZerosSixLines);
        assertEquals(
                "[\": 2], [/: 1], [0: 3], [;: 1], [a: 4]," +
                        " [d: 3], [f: 1], [n: 1], [s: 3], [w: 1], [x: 2], ", analyzer.analyze());
        analyzer = new CountCharsAnalyzer(fiveZerosFiveLines);
        assertEquals("[0: 5], [3: 1], [4: 1], [a: 4], [d: 2], [l: 1], [n: 1], [s: 3], ", analyzer.analyze());

        analyzer = new CountCharsAnalyzer(tenZerosOneLine);
        assertEquals("[0: 10], ", analyzer.analyze());
    }
}

