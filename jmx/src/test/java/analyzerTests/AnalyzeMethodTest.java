package analyzerTests;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

abstract class AnalyzerTest {

    static List<String> threeEmptyLines;
    static List<String> threeZerosSixLines;
    static List<String> fiveZerosFiveLines;
    static List<String> tenZerosOneLine;
    static List<String> emptyFile;
    private static ClassLoader classLoader;

    @BeforeAll
    static void prepareData() throws IOException {
        classLoader = AnalyzerTest.class.getClassLoader();
        threeEmptyLines = getResAsStringList("3EmptyLines");
        threeZerosSixLines = getResAsStringList("3Zeros6Lines");
        fiveZerosFiveLines = getResAsStringList("5Zeros5Lines");
        tenZerosOneLine = getResAsStringList("10Zeros1Line");
        emptyFile = getResAsStringList("emptyFile");
    }

    public static List<String> getResAsStringList(String res) throws IOException {
        return Files.lines(Paths.get(classLoader.getResource(res)
                .getPath())).collect(Collectors.toList());
    }

    @Test
    @DisplayName("analyzer should explode on null data")
    abstract void explodeOnNull();

    @Test
    @DisplayName("analyzer should return 0 on empty file")
    abstract void whenFileIsEmptyReturn0();

    @Test
    @DisplayName("should return correct lines count for each file")
    abstract void countDataCorrectly();
}
