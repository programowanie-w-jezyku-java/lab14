package analyzerTests;

import classes.analyzer.AnalyzerFactory;
import classes.analyzer.CountCharsAnalyzer;
import classes.analyzer.CountLinesAnalyzer;
import classes.analyzer.CountZerosAnalyzer;
import classes.analyzer.Analyzer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class AnalyzerFactoryTest {

    private AnalyzerFactory factory;

    @BeforeEach
    void prepare() {
        factory = new AnalyzerFactory(0);

    }

    @Test
    @DisplayName("should not return null object")
    void nonNullResult() {
        assertNotNull(factory.getAnalyzer(null));
    }

    @Test
    @DisplayName("should return different instances each time")
    void newObjectEeachTime() {
        var a1 = factory.getAnalyzer(null);
        var a2 = factory.getAnalyzer(null);
        assertNotEquals(a1, a2);
    }

    @Test
    @DisplayName("should return every implementation over many tries")
    void allImplementations() {
        var analyzers = new ArrayList<Analyzer>();
        for (int i = 0; i < 100; i++) {
            analyzers.add(factory.getAnalyzer(new ArrayList<>()));
        }
        boolean zero = false,
                chars = false,
                lines = false;
        for (var analyzer : analyzers) {
            if (analyzer instanceof CountZerosAnalyzer) {
                zero = true;
            }
            if (analyzer instanceof CountCharsAnalyzer) {
                chars = true;
            }
            if (analyzer instanceof CountLinesAnalyzer) {
                lines = true;
            }
        }
        assertTrue(zero && chars && lines);
    }
}
