import classes.analyzer.CountZerosAnalyzer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CountZerosTest extends AnalyzerTest {

    @Test
    @DisplayName("analyzer should explode on null data")
    void explodeOnNull() {
        var analyzer = new CountZerosAnalyzer(null);
        assertThrows(NullPointerException.class, analyzer::analyze);
    }

    @Test
    @DisplayName("analyzer should return 0 on empty file")
    void whenFileIsEmptyReturn0() {
        var analyzer = new CountZerosAnalyzer(emptyFile);
        assertEquals(String.format(CountZerosAnalyzer.zerosFormat, 0), analyzer.analyze());
    }

    @Test
    @DisplayName("should return correct zeros count for each file")
    void countDataCorrectly() {
        var analyzer = new CountZerosAnalyzer(fiveZerosFiveLines);
        assertEquals(String.format(CountZerosAnalyzer.zerosFormat, 5), analyzer.analyze());

        analyzer = new CountZerosAnalyzer(tenZerosOneLine);
        assertEquals(String.format(CountZerosAnalyzer.zerosFormat, 10), analyzer.analyze());

        analyzer = new CountZerosAnalyzer(threeZerosSixLines);
        assertEquals(String.format(CountZerosAnalyzer.zerosFormat, 3), analyzer.analyze());
    }

}
