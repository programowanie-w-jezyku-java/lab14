import classes.analyzer.CountLinesAnalyzer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CountLinesTest extends AnalyzerTest {

    @Test
    @DisplayName("analyzer should explode on null data")
    void explodeOnNull() {
        var analyzer = new CountLinesAnalyzer(null);
        assertThrows(NullPointerException.class, analyzer::analyze);
    }

    @Test
    @DisplayName("analyzer should return 0 on empty file")
    void whenFileIsEmptyReturn0() {
        var analyzer = new CountLinesAnalyzer(emptyFile);
        assertEquals(String.format(CountLinesAnalyzer.linesFormat, 0), analyzer.analyze());
    }

    @Test
    @DisplayName("should return correct results for each file")
    void countDataCorrectly() {
        var analyzer = new CountLinesAnalyzer(fiveZerosFiveLines);
        assertEquals(String.format(CountLinesAnalyzer.linesFormat, 5), analyzer.analyze());

        analyzer = new CountLinesAnalyzer(tenZerosOneLine);
        assertEquals(String.format(CountLinesAnalyzer.linesFormat, 1), analyzer.analyze());

        analyzer = new CountLinesAnalyzer(threeZerosSixLines);
        assertEquals(String.format(CountLinesAnalyzer.linesFormat, 6), analyzer.analyze());

        analyzer = new CountLinesAnalyzer(threeEmptyLines);
        assertEquals(String.format(CountLinesAnalyzer.linesFormat, 3), analyzer.analyze());
    }
}
