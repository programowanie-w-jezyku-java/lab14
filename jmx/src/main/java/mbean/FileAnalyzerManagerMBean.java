package mbean;

public interface FileAnalyzerManagerMBean {

    public int getThreadCount();

    public void setThreadCount(int noOfThreads);

    public String getScanPath();

    public void setScanPath(String scanPath);

}