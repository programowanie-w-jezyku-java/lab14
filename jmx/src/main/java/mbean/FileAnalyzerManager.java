package mbean;

import classes.analyzer.AnalyzerFactory;
import classes.fileAnalyzer.FileAnalyzer;

import java.io.IOException;

public class FileAnalyzerManager implements FileAnalyzerManagerMBean {

    private FileAnalyzer fileAnalyzer;

    public FileAnalyzerManager(String scanPath) throws IOException {
        fileAnalyzer = new FileAnalyzer(scanPath, new AnalyzerFactory());
    }

    @Override
    public int getThreadCount() {
        return fileAnalyzer.getThreadCount();
    }

    @Override
    public void setThreadCount(int noOfThreads) {
        fileAnalyzer.setThreadCount(noOfThreads);
    }


    @Override
    public String getScanPath() {
        return fileAnalyzer.getPathToScan();
    }

    @Override
    public void setScanPath(String pathToScan) {
        try {
            fileAnalyzer.setPathToScan(pathToScan);
        } catch (IOException e) {
            System.out.println("Wrong path set!");
        }
    }
}