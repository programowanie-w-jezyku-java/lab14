package classes.fileAnalyzer;

import classes.analyzeMethod.AnalyzeMethod;
import classes.analyzeMethod.AnalyzerFactory;
import classes.memory.FileMemory;

public class Analyzer implements Runnable {
    private final FileMemory memory;
    private AnalyzerFactory factory;
    private boolean stopped = false;
    private int waitTimeInMillis;
    private Thread thread;
    private String threadName = Integer.toString((int) Math.floor(Math.random() * 1000));

    public Analyzer(FileMemory memory, AnalyzerFactory factory, int waitTimeInMillis) {
        this.memory = memory;
        this.factory = factory;
        this.waitTimeInMillis = waitTimeInMillis;
    }

    public void stop() {
        System.out.println("Stopping thread: " + threadName);
        stopped = true;
    }

    public void run() {
        while (!stopped) {
            var loadedFile = memory.loadRandomFile();
            AnalyzeMethod analyzeMethod = factory.getAnalyzer(loadedFile.getContents());
            String result = analyzeMethod.analyze();
            System.out.println("Thread: |" + threadName + "| File:" + loadedFile.getName() + "\n" + result + "\n");
            waiterino();
        }
    }

    private void waiterino() {
        synchronized (this) {
            try {
                wait(waitTimeInMillis);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("Boooom!");
                System.exit(-1);
            }
        }
    }

    public void start() {
        System.out.println("Created new thread!: " + threadName);
        thread = new Thread(this, threadName);
        thread.start();
    }
}
