package classes.fileAnalyzer;

import classes.analyzer.Analyzer;
import classes.analyzer.AnalyzerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class FileAnalyzer {
    private Set<Path> avaiableFiles;
    private AnalyzerFactory factory;
    private String pathToScan;
    private int threadCount = 3;
    private Map<Path, List<String>> loadedFiles;
    private List<Thread> scanThreads;

    public FileAnalyzer(String pathToScan, AnalyzerFactory factory) throws IOException {
        this.factory = factory;
        this.pathToScan = pathToScan;
        avaiableFiles = scanPath(pathToScan);
    }

    private Set<Path> scanPath(String pathToScan) throws IOException {
        return Files.walk(Paths.get(pathToScan))
                .filter(Files::isRegularFile)
                .collect(Collectors.toSet());
    }

    public void run() {
        for (int i = 0; i < threadCount; i++) {
            scanThreads.add(new Thread(this::runAnalyze));
        }
        for (Thread t : scanThreads) {
            t.run();
        }
    }

    private void runAnalyze() {

        try {
            var loadedFile = loadRandomFile();
            Analyzer analyzer = factory.getAnalyzer(loadedFile);
            System.out.println(analyzer.analyze());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Bledna sciezka!!");
        }
    }

    private List<String> loadRandomFile() throws IOException {
        Path fileToAnalyze = avaiableFiles.stream().findAny().orElse(null);
        if (loadedFiles.containsKey(fileToAnalyze)) {
            return loadedFiles.get(loadedFiles);
        } else {
            Path anyKey = loadedFiles.keySet().stream().findAny().orElse(null);
            loadedFiles.remove(anyKey);

            loadedFiles.put(fileToAnalyze, loadFile(fileToAnalyze));
            return loadedFiles.get(fileToAnalyze);
        }
    }

    private List<String> loadFile(Path path) throws IOException {
        return Files.readAllLines(path);
    }

    public String getPathToScan() {
        return pathToScan;
    }

    public void setPathToScan(String pathToScan) throws IOException {
        this.pathToScan = pathToScan;
        scanPath(pathToScan);
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }
}
