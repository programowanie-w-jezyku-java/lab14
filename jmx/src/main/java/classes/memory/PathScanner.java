package classes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PathScanner {
    private Path pathToScan;
    private List<Path> loadedPaths;

    public PathScanner(String path) {
        pathToScan = Paths.get(path);
        if (!pathToScan.toFile().exists())
            throw new InvalidPathException(path, "This is not a valid path!");
    }

    public List<Path> getPaths() {
        if (loadedPaths == null)
            loadedPaths = parsePath();
        return loadedPaths;
    }

    private List<Path> parsePath() {
        try {
            var paths = Files.walk(pathToScan)
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());
            return paths;
        } catch (IOException e) {
            System.out.println("Something bad happened...");
            System.out.println("IOException!");
            System.exit(-1);
        }
        return List.of();
    }

    public String getScanPath() {
        return pathToScan.toString();
    }
}
