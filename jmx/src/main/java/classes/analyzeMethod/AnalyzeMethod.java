package classes.analyzer;

import java.util.List;

public abstract class Analyzer {
    List<String> lines;

    public abstract String analyze();
}
