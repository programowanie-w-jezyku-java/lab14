package classes.analyzer;

import java.util.List;

public class CountLinesAnalyzer extends Analyzer {
    public static final String linesFormat = "lines in file: %d";

    public CountLinesAnalyzer(List<String> lines) {
        this.lines = lines;
    }

    @Override
    public String analyze() {
        int linesCount = 0;
        linesCount = lines.stream()
                .map(x -> 1)
                .reduce((x, acc) -> x + acc).orElse(0);
        return String.format(linesFormat, linesCount);
    }
}
