package classes.analyzer;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class CountZerosAnalyzer extends Analyzer {

    public static final String zerosFormat = "zeros in file: %d";


    public CountZerosAnalyzer(List<String> lines) {
        this.lines = lines;
    }

    @Override
    public String analyze() {
        int zerosCount = 0;
        zerosCount = lines.stream().
                map(line -> StringUtils.countMatches(line, '0')).
                reduce((x, acc) -> x + acc).orElse(0);
        return String.format(zerosFormat, zerosCount);
    }
}
