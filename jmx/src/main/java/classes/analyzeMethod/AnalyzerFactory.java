package classes.analyzer;


import java.util.List;
import java.util.Random;

public class AnalyzerFactory {
    Random random;

    public AnalyzerFactory() {
        random = new Random();
    }

    public AnalyzerFactory(int seed) {
        random = new Random(seed);
    }

    public Analyzer getAnalyzer(List<String> lines) {
        int random = this.random.nextInt(3);
        Analyzer analyzer;
        switch (random) {
            case 0: {
                analyzer = new CountZerosAnalyzer(lines);
                break;
            }
            case 1: {
                analyzer = new CountCharsAnalyzer(lines);
                break;
            }
            default: {
                analyzer = new CountLinesAnalyzer(lines);
                break;
            }
        }
        return analyzer;
    }
}
