package classes.analyzer;

import java.util.*;
import java.util.stream.Collectors;

public class CountCharsAnalyzer extends Analyzer {
    public CountCharsAnalyzer(List<String> lines) {
        this.lines = lines;
    }

    @Override
    public String analyze() {
        var charMap = lines.stream().map(String::toLowerCase)
                .map(x -> x.split(""))
                .flatMap(Arrays::stream)
                .filter(x -> !x.equals(""))
                .collect(Collectors.groupingBy(x -> x, Collectors.counting()));

        return processResultsMap(charMap);
    }

    private String processResultsMap(Map<String, Long> result) {
        var resultsList = new ArrayList<String>();

        for (var character : result.keySet()) {
            Long count = result.get(character);
            resultsList.add(String.format("[%s: %d], ", character, count));
        }
        String formattedResult = resultsList.stream()
                .sorted(Comparator.naturalOrder())
                .reduce("", (acc, a) -> acc + a);
        return formattedResult.equals("") ? "Pusty plik!" : formattedResult;
    }
}
