package exceptions;

public class FileNonExistentException extends Exception {
    public FileNonExistentException(String msg) {
        super(msg);
    }
}
