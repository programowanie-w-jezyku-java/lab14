package main;

import mbean.FileAnalyzerManager;

import javax.management.*;
import java.lang.management.ManagementFactory;

public class App {
    private static String DEFAULT_SCAN_PATH = "~/";

    public static void main(String[] args) throws MalformedObjectNameException, InterruptedException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = new ObjectName("blash.mbean:type=FileAnalyzerManager");

        FileAnalyzerManager mBean = new FileAnalyzerManager(DEFAULT_SCAN_PATH);
        mbs.registerMBean(mBean, name);
        do {
            Thread.sleep(3000);
            System.out.printf("Thread Count: %d, path scanned: %s\n", mBean.getThreadCount(), mBean.getScanPath());
        } while (mBean.getThreadCount() != 0);

    }
}
